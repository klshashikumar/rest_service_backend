# BackEnd REST API


## Requirements

* python 3
* Django 1.11
* postgresql( I have used https://api.elephantsql.com)

## Creating a project using Django
```bash
django-admin startproject BanksBackEnd
```
## Creating a app using Django

```
django-admin startapp branch
```
or
```bash
python manage.py startapp branch
```

## Step 1 

change the DATABASES value in settings.py

```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'database_name',
        'USER':'username',
        'PASSWORD':'passworrd',
        'HOST':'hostname',
        'PORT':'5432',
    }
}
```
## Step2
Create a Model(for branch table)

```python
from django.db import models

class Branches(models.Model):
    ifsc = models.CharField(max_length=11, primary_key=True)
    bank_id = models.BigIntegerField(blank=True)
    branch = models.CharField(max_length=74, blank=True, null=True)
    address = models.CharField(max_length=195, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    district = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=26, blank=True, null=True)

    def __str__(self):
        return self.branch
    class Meta:
        managed = False
        db_table = 'branches'
```
### migrate to change database

```bash
python manage.py makemigrations
python manage.py migrate
```

## Step3
### Serializer

Create the Bank Serializer class
```python

class bankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Branches
        fields = ['ifsc','bank_id','branch','address','city','district','state']
        
```

## Step4
Create a View 

```python 
from django.shortcuts import render
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . models import Branches
from . serializers import bankSerializer
from django_filters.rest_framework import DjangoFilterBackend

# Create your views here.
class bankList(APIView):
    def post(self):
        pass
    def get(self,request):
        branch1 = Branches.objects.all()
        ifsc_q = self.request.query_params.get('ifsc?q','')
        bankid_q = self.request.query_params.get('bank_id?q','')
        branch_q = self.request.query_params.get('branches?q','')
        addr_q= self.request.query_params.get('address?q','')
        city_q = self.request.query_params.get('city?q','')
        district_q = self.request.query_params.get('district?q','')
        state_q = self.request.query_params.get('state?q','')
        autocomplete_q = self.request.query_params.get('autocomplete?q','')

        #default seraializer
        serializer = bankSerializer(branch1,many=True)

        #checks for all rows and columns
        if city_q:
            serializer = bankSerializer(branch1.filter(city__icontains=city_q),many=True)
            return Response(serializer.data)
        if ifsc_q:
            serializer = bankSerializer(branch1.filter(ifsc__icontains=ifsc_q),many=True)
            return Response(serializer.data)
        if bankid_q:
            serializer = bankSerializer(branch1.filter(bank_id__icontains=bankid_q),many=True)
            return Response(serializer.data)
        if branch_q:
            serializer = bankSerializer(branch1.filter(branch__icontains=branch_q),many=True)
            return Response(serializer.data)
        if addr_q:
            serializer = bankSerializer(branch1.filter(address__icontains=addr_q),many=True)
            return Response(serializer.data)
        if district_q:
            serializer = bankSerializer(branch1.filter(district__icontains=district_q),many=True)
            return Response(serializer.data)
        if state_q:
            serializer = bankSerializer(branch1.filter(state__icontains=state_q),many=True)
            return Response(serializer.data)

```
### for autocomplete query for all columns in the table.
```python

        #autocomplete
        if autocomplete_q:
            serializer = bankSerializer(branch1.filter(city__icontains=autocomplete_q),many=True)
            if serializer.data!=[]:
                return Response(serializer.data)
            serializer = bankSerializer(branch1.filter(ifsc__icontains=autocomplete_q),many=True)
            if serializer.data!=[]:
                return Response(serializer.data)
            serializer = bankSerializer(branch1.filter(bank_id__icontains=autocomplete_q),many=True)
            if serializer.data!=[]:
                return Response(serializer.data)
            serializer = bankSerializer(branch1.filter(branch__icontains=autocomplete_q),many=True)
            if serializer.data!=[]:
                return Response(serializer.data)
            serializer = bankSerializer(branch1.filter(address__icontains=autocomplete_q),many=True)
            if serializer.data!=[]:
                return Response(serializer.data)
            serializer = bankSerializer(branch1.filter(district__icontains=autocomplete_q),many=True)
            if serializer.data!=[]:
                return Response(serializer.data)
            serializer = bankSerializer(branch1.filter(state__icontains=autocomplete_q),many=True)
            if serializer.data!=[]:
                return Response(serializer.data)
        return Response(serializer.data)
```

## Step5
#### Add appname and rest_framework 

```python

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework',
    'branch',
]
```
#### Add url pattern of the application to admins.urls.py

```python
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api/',views.bankList.as_view()),
]
```
#### Add Static_root in settings.py
```python
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
```
## Step6
#### Change ALLOWED_HOSTS in settings.py

```python
ALLOWED_HOSTS = []
```
TO

```python
ALLOWED_HOSTS = ['0.0.0.0', 'localhost', 'https://bankwebapp2.herokuapp.com']
```


## Step7
### Hosting in heroku

*  heroku login
*  heroku create bankwebapp2
*  git init
*  git add .
*  git commit -m "add all files"
*  git push heroku master

