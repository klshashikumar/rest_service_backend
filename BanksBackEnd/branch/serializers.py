from rest_framework import serializers
from . models import Branches

class bankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Branches
        fields = ['ifsc','bank_id','branch','address','city','district','state','bank_name']
        